<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tasks Table</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
  <h1>Task App</h1>
  <div class="topnav">
    <a class="active" href='/index'>Index</a>
    <a href='/create'>Create a Task</a>
    <a href='/login'>Login</a>
    <a href='/signup'>Create an Account</a>
  </div> 
  <div class="container">
    @yield('main')
  </div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>
