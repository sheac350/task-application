@extends('base')

@section('main')

    <form action="{{route('signin')}}" method="post" class="form-signin">
      @csrf
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputUsernmae" class="sr-only">Username</label>
      <input type="text" name="username" id="inputUsernmae" class="form-control" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>

    <div class="text-center">

      @if(session()->get('unsuccessful'))
      <div class="alert alert-unsuccessful">
        {{ session()->get('unsuccessful') }}  
      </div>
      @endif
    </div>

    @endsection
