@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
  <h1 class="display-3">Add a task</h1>
  <div>
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div><br />
    @endif
    <form method="post" action="{{ route('createtask') }}">
      @csrf
      <div class="form-group">    
        <label for="task_date">Task Date: (select the calendar icon)</label>
        <input type="date" class="form-control" name="task_date"/>
      </div>

      <div class="form-group">
        <label for="task_info">Task Info:</label>
        <input type="text" class="form-control" name="task_info"/>
      </div>
      
      <button type="submit" class="btn btn-dark">Add task</button>
    </form>
  </div>
</div>
</div>
@endsection