@extends('base')

@section('main')
<div class="row">
  <div class="col-sm-12">
    <h1 class="display-3">Tasks</h1>    
    <table class="table table-striped">
      <thead>
        <tr>
          <td>ID</td>
          <td>Task Date</td>
          <td>Task Info</td>
          <td colspan = 2>Actions</td>
        </tr>
      </thead>
      <tbody>
        @foreach($tasks as $task)
        <tr>
          <td>{{$task->id}}</td>
          <td>{{$task->task_date}}</td>
          <td>{{$task->task_info}}</td>
          <td>
            <a href="{{ route('edittask',$task->id)}}" class="btn btn-primary">Edit</a>
          </td>
          <td>
            <form action="{{ route('destroytask', $task->id)}}" method="post">
              @csrf
              @method('POST')
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>    
    <div class="col-sm-12">

      @if(session()->get('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}  
      </div>
      @endif
    </div>

    {{ $tasks->links() }}

    @endsection
