<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;


Route::get('/', function () {
    return view('base');
});

Route::get('/create', 'App\Http\Controllers\TaskController@create');
Route::post('/create', 'App\Http\Controllers\TaskController@store')->name('createtask');

Route::get('/index', 'App\Http\Controllers\TaskController@index');

Route::get('/edit/{id}', 'App\Http\Controllers\TaskController@edit')->name('edittask');

Route::post('/destroy/{id}', 'App\Http\Controllers\TaskController@destroy')->name('destroytask');

Route::post('/update/{id}', 'App\Http\Controllers\TaskController@update')->name('updatetask');

Route::get('/login', 'App\Http\Controllers\LoginController@login')->name('login');
Route::post('/login', 'App\Http\Controllers\LoginController@signin')->name('signin');

Route::get('/signup', 'App\Http\Controllers\LoginController@signup')->name('signup');
Route::post('/signup', 'App\Http\Controllers\LoginController@createuser')->name('createuser');