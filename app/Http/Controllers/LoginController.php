<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
	public function login()
	{
		return view('login');
	}

	public function signup()
	{
		return view('signup');
	}


	public function createuser(Request $request)
	{
		$user =new User;
		$user->username = $request->username;
		$user->password = Hash::make($request->password);
		$user->save();

		return redirect('/index')->with('success', 'New user saved!');
	}


	public function signin(Request $request)
	{


		$credentials = $request->only('username', 'password');


		if (Auth::attempt($credentials)) {
            return redirect('/index')->with('success', 'You are now logged in!');
        }
		else
		{
			return redirect()->back()->with('unsuccessful', 'Incorrect login details.');
		}
	}
}


